======
README
======

Wambenger is a small blogging platform utilizing jinja2 for templating. It's goal is to provide an option for generating a static HTML blog from plaintext files.

|
|

-----
Setup
-----

Content source files should be created with the following layout (files marked with a * are
required)::

    <site-name>/
      `- * header.md
      `- footer.md
      `- sidebar.md
      `- index.html
      `- stylesheet.css
      `- sections/
          `- <section-name>/
              `- article.html
              `- by_tag.html
              `- index.html
              `- * info.md
              `- articles/
              |   `- <first article>.md
              |   `- <second article>.md
              `- resources/
                  `- <image>.png
                  `- <audio>.mp3


Upon generation, the following structure is generated::

    <output-folder>/
      `- index.html
      `- resources/
      |   `- <image>.png
      |   `- <audio>.mp3
      `- <section1-slug>/
          `- index.html
          `- <article1-slug>.html
          `- <article2-slug>.html
          `- by_tag/
              `- <tag1>.html
              `- <tag2>.html

By default, sections are sorted alphabetically by title and articles are sorted by publish_date
with the newest on top. This can be changed by adding an `index` tag to the relavant `.md` files.
Any articles or sections with indexes defined will take precedence over articles which lack them,
effectively "pinning" them to the top.

Sections and articles can also be hidden by setting the 'hidden' tag to 'True' in the relavant
`.md` file. This will cause sections to not be displayed in the main index (though they will still
be listed in the nav bar, if used) and articles to not be displayed in their parent sections
or tag pages (though they will still be viewable with a direct link).

|
|

-------------
Configuration
-------------

|
|

-----
Usage
-----

~~~~~~~~~~~~~~~~~~~~~~
Importing as a Library
~~~~~~~~~~~~~~~~~~~~~~

|
|

::
>>> from wambenger.classes import Runner
>>> Runner(input_directory='input/', output_directory='/var/www/').write_all()

-----
To-Do
-----

 - Write a custom `jinja2.BaseLoader` instead of the current hacky solution
 - Generate an atom feed
 - copy stylesheet to output directory
 - media handling
 - Utilize microformats https://indieweb.org/microformats
 - Cache previously generated articles
 - Generate an XML sitemap
 - Gemini support
 - Gopher support

|
|

----------------
Acknowledgements
----------------


