#!/usr/bin/var python
# -*- coding: utf-8 -*-

import sys
from setuptools import setup
from glob import glob


def readme():
    with open('README.rst') as f:
        return f.read()


try:
    from wambenger.version import __version__
except ImportError:
    pass

exec(open('wambenger/version.py').read())


setup(
    name='wambenger',
    version=__version__,
    description='Small python blogging platform utilizing jinja2',
    long_description=readme(),
    long_description_content_type='text/x-rst',
    author='c0nch0b4r',
    author_email='lp1.on.fire@gmail.com',
    packages=[
        'wambenger'
    ],
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8'
    ],
    keywords='blogging',
    url='https://bitbucket.org/c0nch0b4r/wambenger',
    download_url='https://bitbucket.org/c0nch0b4r/wambenger/get/' + __version__ + '.tar.gz',
    project_urls={
        'Source': 'https://bitbucket.org/c0nch0b4r/wambenger/src'
    },
    python_requires='>=3.8, <4',
    install_requires=[
        'typing',
        'requests',
        'appdirs',
        'jinja2',
        'markdown',
        'feedgen',
        'pendulum'
    ],
    entry_points={
        'console_scripts': ['wambenger=wambenger.starter:main'],
    }
)
