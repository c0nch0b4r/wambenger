#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import *

import argparse
import logging
import pendulum
import re
import textwrap
from unicodedata import normalize

from wambenger.version import __version__

# Values taken from:
#    https://scholarwithin.com/average-reading-speed#average-reading-speed-by-age-and-grade
WORDS_PER_MINUTE_MIN = 220
WORDS_PER_MINUTE_MAX = 350
WORDS_PER_MINUTE_AVG = (WORDS_PER_MINUTE_MAX + WORDS_PER_MINUTE_MIN) / 2

# Max reading time in minutes. TO-DO: Make this configurable, maybe in header.md?
READING_TIME_MAX = 40
# icons to use for reading time, scaled from 0 to the max above
READING_TIME_ICONS = [
    '🕐', '🕜', '🕑', '🕝', '🕒', '🕞', '🕓', '🕟', '🕔', '🕠', '🕕', '🕡',
    '🕖', '🕢', '🕗', '🕣', '🕘', '🕤', '🕙', '🕥', '🕚', '🕦', '🕛', '🕧'
]

# Value taken from:
#    https://www.researchgate.net/figure/Average-word-length-in-the-English-language-Different-colours-indicate-the-results-for_fig1_230764201
AVERAGE_WORD_LENGTH = 5

def create_slug(text: str, max_length: int = 50) -> str:
    allowed_characters = "abcdefghijklmnopqrstuvwxyz0123456789-_"
    text = textwrap.wrap(text=text, width=max_length, break_long_words=False, placeholder='-')
    text = text[0].strip().replace(' ', '-').lower()
    text = normalize('NFKD', text)
    text = map(lambda x: x if x in allowed_characters else '', text)
    text = ''.join(text)
    return text

def clamp_and_scale(
    old_min_value: float,
    old_max_value: float,
    new_min_value: float,
    new_max_value: float,
    current_value: float
) -> float:
    if current_value > old_max_value:
        old_max_value = current_value
    assert old_min_value <= current_value <= old_max_value
    assert new_min_value < new_max_value
    old_range = old_max_value - old_min_value
    new_range = new_max_value - new_min_value
    scaled_value = (((current_value - old_min_value) * new_range) / old_range) + new_min_value
    return scaled_value

def generate_text_metadata(text_body: str) -> Dict[str, Union[str, int, float]]:
    word_list = re.findall(r'\w+', text_body)
    word_count = len(word_list)
    total_length = sum(len(word) for word in word_list)

    total_average = 0
    for current_word in word_list:
        total_average += len(current_word) / AVERAGE_WORD_LENGTH
        #total_average += len(current_word) / word_length_average

    word_length_average = total_length / word_count
    read_min = total_average / WORDS_PER_MINUTE_MIN
    read_avg = total_average / WORDS_PER_MINUTE_AVG
    read_max = total_average / WORDS_PER_MINUTE_MAX
    icon_index = round(clamp_and_scale(0, READING_TIME_MAX, 0, len(READING_TIME_ICONS), read_avg))

    metadata = {
        'word_count': word_count,
        'word_count_average': total_average,
        'word_length_average': word_length_average,
        'reading_time_min': pendulum.duration(minutes=read_min),
        'reading_time_avg': pendulum.duration(minutes=read_avg),
        'reading_time_max': pendulum.duration(minutes=read_max),
        'reading_time_icon': '📖' + READING_TIME_ICONS[icon_index]
    }

    return metadata

def get_options() -> dict:
    parser = argparse.ArgumentParser(
        prog='wambenger',
        description="Static site generator",
        fromfile_prefix_chars='@'
    )
    parser.add_argument(
        "-V",
        "--version",
        action="version",
        version="%(prog)s {}".format(__version__)
    )
    parser.add_argument(
        "-d",
        "--debug",
        action="store_true",
        dest="debug_logging",
        help="Show debug messages"
    )
    parser.add_argument(
        "-c",
        "--config",
        action="store",
        dest="config_file",
        help="Config file path if not default (`~/.config/wambenger/config.json`)",
        default='~/.config/wambenger/config.json'
    )

    parser.add_argument(
        "-i",
        "--input",
        action="store",
        dest="input_directory",
        help="Input directory for source markdown files",
        default='input'
    )

    parser.add_argument(
        "-b",
        "--base_path",
        action="store",
        dest="base_path",
        help="Base path for links (default: `/`)"
    )
    parser.add_argument(
        "-H",
        "--html_output",
        action="store",
        dest="html_output",
        help="Output directory for generated HTML files"
    )
    parser.add_argument(
        "-g",
        "--gopher_output",
        action="store",
        dest="gopher_output",
        help="Output directory for generated gophermap files"
    )
    parser.add_argument(
        "-G",
        "--gemini_output",
        action="store",
        dest="gemini_output",
        help="Output directory for generated gemtext files"
    )

    parser.add_argument(
        "-r",
        "--rss",
        action="store_true",
        dest="generate_rss",
        help="Generate rss/atom feed output"
    )
    parser.add_argument(
        "-e",
        "--delete",
        action="store_true",
        dest="delete",
        help="Delete pre-existing files in output directories"
    )
    parser.add_argument(
        "-s",
        "--symlink_resources",
        action="store_true",
        dest="symlink_resources",
        help="Symlink resources instead of copying"
    )

    parser.add_argument(
        "-w",
        "--web-server",
        action="store_true",
        dest="web_server",
        help="Run a web server from output directory (NOT FOR PRODUCTION USE)"
    )

    parser.add_argument(
        "-D",
        "--daemon",
        action="store_true",
        dest="daemon",
        help="Run wambenger as a daemon"
    )

    options = vars(parser.parse_args())
    if options['debug_logging']:
        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s [%(levelname)-7s] %(message)s')
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s')
    del options['debug_logging']

    return options
