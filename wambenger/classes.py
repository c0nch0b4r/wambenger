#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import annotations

import datetime
import io
import jinja2
import json
import logging
import markdown
import pathlib
import shutil
from feedgen.feed import FeedGenerator
from typing import Any, Dict, List, Optional, TYPE_CHECKING, Union

from .utils import create_slug, generate_text_metadata
from .version import __version__

if TYPE_CHECKING:
    pass


class Article(object):
    def __init__(
            self,
            source_file: str,
            title: str,
            publish_date: datetime.datetime,
            body: str,
            hidden: bool = False,
            index_number: Optional[int] = None,
            subtitle: Optional[str] = None,
            tags: Optional[List[str]] = None,
            update_date: Optional[datetime.datetime] = None,
            author: Optional[str] = None,
            author_email: Optional[str] = None,
            slug: Optional[str] = None
            ) -> None:
        self.source_file = source_file
        self.title = title
        self.publish_date = publish_date
        self.body = body
        self.hidden = hidden
        self.index_number = index_number
        self.subtitle = subtitle
        self.tags = tags
        self.update_date = update_date
        self.author = author
        self.author_email = author_email
        if slug:
            self.slug = slug
        else:
            self.slug = create_slug(self.title)
        self.metadata = generate_text_metadata(self.body)

    def regenerate(self) -> None:
        md = MarkdownObject.from_file(self.source_file)
        self.title = md.metadata['title'][0]
        self.publish_date = datetime.datetime.strptime(md.metadata['publish_date'][0], '%Y-%m-%d %H:%M:%S').astimezone()
        self.hidden = False
        if 'hidden' in md.metadata and md.metadata['hidden'][0] == 'True':
            self.hidden = True
        if 'index' in md.metadata:
            self.index_number = int(md.metadata['index'][0])
        subtitle = None
        if 'subtitle' in md.metadata:
            subtitle = md.metadata['subtitle'][0]
        self.subtitle = subtitle
        tags = []
        if 'tags' in md.metadata:
            for tagset in md.metadata['tags']:
                tags.extend(tagset.split(', '))
        self.tags = tags
        update_date = None
        if 'update_date' in md.metadata:
            update_date = datetime.datetime.strptime(md.metadata['update_date'][0], '%Y-%m-%d %H:%M:%S').astimezone()
        self.update_date = update_date
        author = None
        if 'author' in md.metadata:
            author = md.metadata['author'][0]
        self.author = author
        author_email = None
        if 'author_email' in header_md.metadata:
            author_email = header_md.metadata['author_email'][0]
        self.author_email = author_email
        self.body = md.body

    @classmethod
    def from_file(cls, source_file: str) -> Article:
        md = MarkdownObject.from_file(source_file)
        publish_date = datetime.datetime.strptime(md.metadata['publish_date'][0], '%Y-%m-%d %H:%M:%S').astimezone()
        hidden = False
        if 'hidden' in md.metadata and md.metadata['hidden'][0] == 'True':
            hidden = True
        index_number = None
        if 'index' in md.metadata:
            index_number = int(md.metadata['index'][0])
        tags = []
        if 'tags' in md.metadata:
            for tagset in md.metadata['tags']:
                tags.extend(tagset.split(', '))
        update_date = None
        if 'update_date' in md.metadata:
            update_date = datetime.datetime.strptime(md.metadata['update_date'][0], '%Y-%m-%d %H:%M:%S').astimezone()
        author = None
        if 'author' in md.metadata:
            author = md.metadata['author'][0]
        author_email = None
        if 'author_email' in md.metadata:
            author_email = md.metadata['author_email'][0]
        subtitle = None
        if 'subtitle' in md.metadata:
            subtitle = md.metadata['subtitle'][0]
        return cls(
            source_file = source_file,
            title = md.metadata['title'][0],
            subtitle = subtitle,
            publish_date = publish_date,
            body = md.body,
            hidden = hidden,
            index_number = index_number,
            tags = tags,
            update_date = update_date,
            author = author,
            author_email = author_email
        )


class Section(object):
    def __init__(
            self,
            source_dir: str,
            title: str,
            slug: str,
            body: str,
            hidden: bool = False,
            index_number: Optional[int] = None,
            articles: Optional[List[Article]] = None
            ) -> None:
        self.source_dir = source_dir
        self.title = title
        self.slug = slug
        self.body = body
        self.index_number = index_number
        self.hidden = hidden
        self.articles = articles
        self.articles_sorted = []
        self._sort_articles()

    def regenerate(self) -> None:
        self.articles = []
        source_dir = pathlib.Path(self.source_dir)
        md = MarkdownObject.from_file(str(source_dir / 'info.md'))
        self.title = md.metadata['title'][0]
        self.slug = md.metadata['slug'][0]
        if 'hidden' in md.metadata and md.metadata['hidden'][0] == 'True':
            self.hidden = True
        if 'index' in md.metadata:
            self.index_number = int(md.metadata['index'][0])
        self.articles = []
        articles_dir = source_dir / 'articles'
        for article_file in articles_dir.rglob('*.md'):
            self.articles.append(Article.from_file(str(article_file)))

    def _sort_articles(self) -> None:
        with_index = []
        without_index = []
        for article in self.articles:
            if article.index_number:
                with_index.append(article)
            else:
                without_index.append(article)
        with_index.sort(key=lambda x: x.index_number)
        without_index.sort(key=lambda x: x.publish_date, reverse=True)
        self.articles_sorted = [*with_index, *without_index]

    @classmethod
    def from_directory(cls, source_dir: pathlib.Path) -> Section:
        source_dir = pathlib.Path(source_dir)
        try:
            md = MarkdownObject.from_file(str(source_dir / 'info.md'))
        except FileNotFoundError:
            logging.error(f'No info.md file found at `{source_dir}`!')
            raise FileNotFoundError
        hidden = False
        if 'hidden' in md.metadata and md.metadata['hidden'][0] == 'True':
            hidden = True
        index_number = None
        if 'index' in md.metadata:
            index_number = int(md.metadata['index'][0])
        articles = []
        articles_dir = source_dir / 'articles'
        for article_file in articles_dir.rglob('*.md'):
            new_article = Article.from_file(str(article_file))
            if 'draft' in new_article.tags:
                continue
            articles.append(new_article)
            #articles.append(Article.from_file(str(article_file)))
        return cls(
            source_dir = source_dir,
            title = md.metadata['title'][0],
            slug = md.metadata['slug'][0],
            body = md.body,
            hidden = hidden,
            index_number = index_number,
            articles = articles
        )


class Nav(object):
    def __init__(
            self,
            text: str,
            base_path: str = '/'
            ):
        self.text = text
        self.base_path = base_path

    @property
    def href(self):
        return f'{self.base_path}{self.text}/index.html'

class Index(object):
    def __init__(
            self,
            source_dir: str,
            sections: List[Section],
            site: str,
            title: str,
            subtitle: Optional[str] = None,
            author: Optional[str] = None,
            author_email: Optional[str] = None,
            header: Optional[str] = None,
            footer: Optional[str] = None,
            sidebar: Optional[str] = None
            ) -> None:
        self.source_dir = source_dir
        self.sections = sections
        self.site = site
        self.title = title
        self.subtitle = subtitle
        self.author = author
        self.author_email = author_email
        self.header = header
        self.footer = footer
        self.sidebar = sidebar
        self.nav = None
        self.sections_sorted = None
        self._sort_sections()

    def parse_sections(self) -> None:
        pass

    def parse_info(self) -> None:
        pass

    def generate_index_html(self) -> str:
        pass

    def _sort_sections(self) -> None:
        with_index = []
        without_index = []
        for section in self.sections:
            if section.index_number:
                with_index.append(section)
            else:
                without_index.append(section)
        with_index.sort(key=lambda x: x.index_number)
        without_index.sort(key=lambda x: x.title)
        self.sections_sorted = [*with_index, *without_index]

    @classmethod
    def from_path(
        cls,
        source_dir: Union[str,pathlib.Path]
    ) -> Index:
        if isinstance(source_dir, str):
            source_dir = pathlib.Path(source_dir).expanduser().resolve()
        header_md = MarkdownObject.from_file(str(source_dir / 'header.md'))
        footer = None
        try:
            footer_md = MarkdownObject.from_file(str(source_dir / 'footer.md'))
        except FileNotFoundError:
            logging.info('No footer.md file found')
        else:
            footer = footer_md.body
        sidebar = None
        try:
            sidebar_md = MarkdownObject.from_file(str(source_dir / 'sidebar.md'))
        except FileNotFoundError:
            logging.info('No sidebar.md found')
        else:
            sidebar = sidebar_md.body
        sections_dir = source_dir / 'sections'
        sections = []
        for section_dir in sections_dir.iterdir():
            if not section_dir.is_dir():
                continue
            sections.append(Section.from_directory(str(section_dir)))
        subtitle = None
        if 'subtitle' in header_md.metadata:
            subtitle = header_md.metadata['subtitle'][0]
        author = None
        author_email = None
        if 'author' in header_md.metadata:
            author = header_md.metadata['author'][0]
        if 'author_email' in header_md.metadata:
            author_email = header_md.metadata['author_email'][0]
        return cls(
            source_dir = source_dir,
            sections = sections,
            site = header_md.metadata['site'][0],
            title = header_md.metadata['title'][0],
            subtitle = subtitle,
            author = author,
            author_email = author_email,
            header = header_md.body,
            footer = footer,
            sidebar = sidebar
        )

class Runner(object):
    def __init__(
            self,
            input_directory: str,
            html_output: Optional[str] = None,
            gopher_output: Optional[str] = None,
            gemini_output: Optional[str] = None,
            base_path: Optional[str] = None,
            generate_rss: bool = False,
            delete: bool = False,
            symlink_resources: bool = False,
            *args,
            **kwargs
            ) -> None:
        self.input_directory = pathlib.Path(input_directory).expanduser().resolve()

        self.html_output = None
        if html_output:
            self.html_output = pathlib.Path(html_output).expanduser().resolve()

        self.gopher_output = None
        if gopher_output:
            self.gopher_output = pathlib.Path(gopher_output).expanduser().resolve()

        self.gemini_output = None
        if gemini_output:
            self.gemini_output = pathlib.Path(gemini_output).expanduser().resolve()

        if base_path is None:
            base_path = '/'
        self.base_path = base_path
        if not self.html_output and not self.gopher_output and not self.gemini_output:
            logging.exception('No output specified! Quitting.')
            raise AttributeError
        self.rss = generate_rss
        self.delete = delete
        self.symlink_resources = symlink_resources
        self.index = Index.from_path(self.input_directory)
        self._jinja_templates = {}
        self._jinja_env = None
        self._jinja_loader = None
        self._set_default_author()
        self._collect_templates()

    def _create_output_dirs(self) -> None:
        for directory in [self.html_output, self.gopher_output, self.gemini_output]:
            if directory is None:
                continue
            directory.mkdir(parents=True, exist_ok=True)

    def _link_extras(self) -> None:
        resource_dir = self.input_directory / 'resources'
        try:
            if self.html_output:
                (self.html_output / 'resources').symlink_to(resource_dir, target_is_directory=True)
            if self.gopher_output:
                (self.gopher_output / 'resources').symlink_to(resource_dir, target_is_directory=True)
            if self.gemini_output:
                (self.gemini_output / 'resources').symlink_to(resource_dir, target_is_directory=True)
        except FileExistsError:
            logging.info('Target resource directory already exists, skipping symlink.')

    def _copy_extras(self) -> None:
        resource_dir = self.input_directory / 'resources'
        if not resource_dir.exists:
            return
        if self.html_output:
            (self.html_output / 'resources').mkdir(exist_ok=True)
            shutil.copytree(src=resource_dir, dst=self.html_output / 'resources', dirs_exist_ok=True)
        if self.gopher_output:
            (self.gopher_output / 'resources').mkdir(exist_ok=True)
            shutil.copytree(src=resource_dir, dst=self.gopher_output / 'resources', dirs_exist_ok=True)
        if self.gemini_output:
            (self.gemini_output / 'resources').mkdir(exist_ok=True)
            shutil.copytree(src=resource_dir, dst=self.gemini_output / 'resources', dirs_exist_ok=True)

    def _delete_existing(self) -> None:
        file_count = 0
        if self.html_output:
            for file in self.html_output.glob('**/*.html'):
                logging.debug(f'Deleting file `{file}`')
                file.unlink()
                file_count += 1
        if self.gopher_output:
            for file in self.gopher_output.glob('**/*.gophermap'):
                logging.debug(f'Deleting file `{file}`')
                file.unlink()
                file_count += 1
        if self.gemini_output:
            for file in self.gemini_output.glob('**/*.gmi'):
                logging.debug(f'Deleting file `{file}`')
                file.unlink()
                file_count += 1
        logging.info(f'Deleted {file_count} files')

    def _collect_templates(self) -> None:
        temp_jinja_env = jinja2.Environment()
        package_loader = jinja2.PackageLoader('wambenger', 'templates')
        all_templates = {}
        for basename in ['index', 'article', 'section', 'by_tag']:
            for extension in ['html', 'gophermap', 'gmi', 'atom']:
                selector = f'{basename}.{extension}'
                template_file = pathlib.Path(self.index.source_dir) / selector
                if template_file.exists():
                    with open(template_file, 'rb') as ifp:
                        all_templates[selector] = ifp.read().decode('utf-8')
                else:
                    try:
                        all_templates[selector] = package_loader.get_source(temp_jinja_env, selector)[0]
                    except Exception:
                        logging.debug(f'No template for {selector}, skipping')

                for section in self.index.sections:
                    section_specific = f'{section.slug}_{selector}'
                    section_index_template = pathlib.Path(section.source_dir) / selector
                    if section_index_template.exists():
                        with open(section_index_template, 'rb') as sfp:
                            all_templates[f'{section_specific}'] = sfp.read().decode('utf-8')
        self._jinja_env = jinja2.Environment(trim_blocks=True, loader=jinja2.DictLoader(all_templates))

    def serve_html(self, host: str = '0.0.0.0', port: int = 9000) -> None:
        # For testing only
        logging.info(f'Running web server on `{host}:{port}`.')
        from http.server import HTTPServer, SimpleHTTPRequestHandler
        import os
        os.chdir(self.html_output)
        http_server = HTTPServer((host, port), SimpleHTTPRequestHandler)
        http_server.serve_forever()

    def write_all(self) -> None:
        if self.delete:
            self._delete_existing()
        self._create_output_dirs()
        if self.symlink_resources:
            self._link_extras()
        else:
            self._copy_extras()
        self.write_index()
        self.write_section()
        self.write_tags()
        self.write_article()
        if self.rss:
            self.write_rss()

    def write_index(self) -> None:
        index_file_template = self._jinja_env.get_template('index.html')
        nav = []
        for section in self.index.sections_sorted:
            nav.append(Nav(section.slug, base_path=self.base_path))
        with open(self.html_output / 'index.html', 'wb') as ifp:
            ifp.write(index_file_template.render(
                site_name = self.index.site,
                title = self.index.title,
                header = self.index.header,
                sidebar = self.index.sidebar,
                sections = self.index.sections,
                sections_sorted = self.index.sections_sorted,
                footer = self.index.footer,
                base_path = self.base_path,
                nav = nav,
                wam_version = __version__,
                gen_datetime = datetime.datetime.now()
            ).encode('utf-8'))

    def write_section(self) -> None:
        nav = []
        for section in self.index.sections_sorted:
            nav.append(Nav(section.slug, base_path=self.base_path))
        for section in self.index.sections:
            try:
                section_index_template = self._jinja_env.get_or_select_template([f'{section.slug}_section.html', 'section.html'])
            except jinja2.exceptions.TemplatesNotFound:
                logging.error(f'Can not locate template for section `{section.title}.html` index!')
            section_file_dir = self.html_output / section.slug
            section_file_dir.mkdir(parents=True, exist_ok=True)
            with open(section_file_dir / 'index.html', 'wb') as ifp:
                ifp.write(section_index_template.render(
                    site_name = self.index.site,
                    title = self.index.title,
                    header = self.index.header,
                    sidebar = self.index.sidebar,
                    section = section,
                    footer = self.index.footer,
                    base_path = self.base_path,
                    nav = nav,
                    wam_version = __version__,
                    gen_datetime = datetime.datetime.now()
                ).encode('utf-8'))

    def write_tags(self) -> None:
        nav = []
        for section in self.index.sections_sorted:
            nav.append(Nav(section.slug, base_path=self.base_path))
        for section in self.index.sections:
            articles_by_tag = {}
            try:
                tags_template = self._jinja_env.get_or_select_template([f'{section.slug}_by_tag.html', 'by_tag.html'])
            except jinja2.exceptions.TemplatesNotFound:
                logging.error(f'Can not locate template for section `{section.title}.html` tags!')
            tags_file_dir = self.html_output / section.slug / 'by_tag'
            tags_file_dir.mkdir(parents=True, exist_ok=True)
            for article in section.articles:
                if 'draft' in article.tags:
                    logging.info(f'Skipping tags from draft article `{article}`.')
                    continue
                for tag in article.tags:
                    if tag not in articles_by_tag:
                        articles_by_tag[tag] = []
                    articles_by_tag[tag].append(article)
            for tag in articles_by_tag:
                with open(tags_file_dir / f'{tag}.html', 'wb') as ifp:
                    ifp.write(tags_template.render(
                        site_name = self.index.site,
                        title = self.index.title,
                        header = self.index.header,
                        sidebar = self.index.sidebar,
                        section = section,
                        matching_articles = articles_by_tag[tag],
                        by_tag = tag,
                        footer = self.index.footer,
                        base_path = self.base_path,
                        nav = nav,
                        wam_version = __version__,
                        gen_datetime = datetime.datetime.now()
                    ).encode('utf-8'))

    def write_article(self) -> None:
        nav = []
        for section in self.index.sections_sorted:
            nav.append(Nav(section.slug, base_path=self.base_path))
        for section in self.index.sections:
            try:
               article_template = self._jinja_env.get_or_select_template([f'{section.slug}_article.html', 'article.html'])
            except jinja2.exceptions.TemplatesNotFound:
                logging.error(f'Can not locate template for section `{section.title}.html` article!')
            article_file_dir = self.html_output / section.slug
            article_file_dir.mkdir(parents=True, exist_ok=True)
            for article in section.articles:
                if 'draft' in article.tags:
                    logging.info(f'Skipping draft article `{article}`.')
                    continue
                with open(article_file_dir / f'{article.slug}.html', 'wb') as ifp:
                    ifp.write(article_template.render(
                        site_name = self.index.site,
                        title = self.index.title,
                        header = self.index.header,
                        sidebar = self.index.sidebar,
                        section = section,
                        article = article,
                        footer = self.index.footer,
                        base_path = self.base_path,
                        nav = nav,
                        wam_version = __version__,
                        gen_datetime = datetime.datetime.now()
                    ).encode('utf-8'))

    def write_rss(self) -> None:
        feed_gen = FeedGenerator()
        feed_gen.id(f'https://{self.index.site}/atom.xml')
        feed_gen.title(self.index.title)
        author_data = {'name': self.index.author}
        if self.index.author_email:
            author_data['email'] = self.index.author_email
        feed_gen.author(author_data)
        feed_gen.link(href=f'https://{self.index.site}', rel='alternate')
        if self.index.subtitle:
            feed_gen.subtitle(self.index.subtitle)
        feed_gen.link(href=f'https://{self.index.site}/atom.xml', rel='self')
        feed_gen.language('en')
        nav = []
        for section in self.index.sections_sorted:
            nav.append(Nav(section.slug, base_path=self.base_path))
        for section in self.index.sections:
            try:
                article_template = self._jinja_env.get_or_select_template([f'{section.slug}_article.html', 'article.atom'])
            except jinja2.exceptions.TemplatesNotFound:
                logging.error(f'Can not locate template for section `{section.title}.atom` article!')
            for article in section.articles:
                if 'draft' in article.tags:
                    logging.info(f'Skipping draft article `{article}`.')
                    continue
                article_url = f'https://{self.index.site}/{section.slug}/{article.slug}.html'
                feed_item = feed_gen.add_entry()
                feed_item.id(
                    self._generate_tag(
                        self.index.site,
                        article.publish_date,
                        f'{section.slug}/{article.slug}'
                    )
                )
                feed_item.title(article.title)
                feed_item.link({'href': article_url})
                feed_item.published(article.publish_date)
                if article.update_date:
                    feed_item.updated(article.update_date)
                feed_item.content(
                    content=article_template.render(
                        site_name = self.index.site,
                        title = self.index.title,
                        header = self.index.header,
                        sidebar = self.index.sidebar,
                        section = section,
                        article = article,
                        footer = self.index.footer,
                        base_path = self.base_path,
                        nav = nav,
                        wam_version = __version__,
                        gen_datetime = datetime.datetime.now()
                    ).encode('utf-8'),
                    type='html'
                )
        feed_gen.atom_file(str(self.html_output / 'atom.xml'))

    def _set_default_author(self) -> None:
        # This is a very hacky way to do it.. fix later (famous last words)
        for section in self.index.sections:
            for article in section.articles:
                if article.author is None:
                    article.author = self.index.author
                else:
                    article.author = 'Anonymous'

    @staticmethod
    def _generate_tag(authority_name: str, date: datetime.datetime, specific: str) -> str:
        """ Generate an RFC4151 tag uri. """
        if date.day == 1 & date.month == 1:
            date = str(date.year)
        elif date.day == 1 & date.month != 1:
            date = date.strftime('%Y-%m')
        else:
            date = date.strftime('%Y-%m-%d')
        return f'tag:{authority_name},{date}:{specific}'


class MarkdownObject(object):
    def __init__(
            self,
            source_file: str,
            body: str,
            metadata: Optional[Dict[str, str]] = None
            ) -> None:
        self.source_file = source_file
        self.body = body
        if not metadata:
            metadata = dict()
        self.metadata = metadata

    @classmethod
    def from_file(cls, file_path: str) -> MarkdownObject:
        parsed_md = cls.parse_markdown(file_path)
        return cls(source_file=file_path, **parsed_md)

    def reparse(self) -> None:
        parsed_md = self.parse_markdown(file_path)
        self.body = parsed_md['body']
        self.metadata = parsed_md['metadata']

    @staticmethod
    def parse_markdown(file_path: str) -> Dict[str, Any]:
        md = markdown.Markdown(extensions=['meta', 'tables', 'markdown_sub_sup'], output_format='html5')
        html_bytes = io.BytesIO()
        md.convertFile(input=file_path, output=html_bytes, encoding='utf-8')
        html_bytes.seek(0)
        body = html_bytes.read().decode('utf-8')
        metadata = md.Meta
        return {
            'body': body,
            'metadata': metadata
        }
