#!/usr/bin/env python
# -*- coding: utf-8 -*-

from wambenger.classes import Runner
from wambenger.utils import get_options


def main() -> None:
    options = get_options()
    runner = Runner(**options)
    runner.write_all()
    if options['web_server']:
        runner.serve_html()


if __name__ == '__main__':
    main()
